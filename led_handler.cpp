#include <thread>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

void blink() {
    unsigned char *mem_base;
    mem_base = (unsigned char*) map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base == NULL) {
        return;
    }

    for (int i = 0; i < 3; i++) {
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0xffffffff;
        std::this_thread::sleep_for(std::chrono::milliseconds(400));
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0x00000000; 
        std::this_thread::sleep_for(std::chrono::milliseconds(400));  
    } 
}
