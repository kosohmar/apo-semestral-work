#include <thread>
#include <chrono>
#include <iostream>
#include <mutex>

#include "presenter.h"

void present_julia(double *imaginary, std::mutex *mutex, bool *end) {
    std::unique_lock <std::mutex> thread_lock(*mutex);
    thread_lock.unlock();
    while(!*end) {
        for(double i = 0.28; i < 0.5; i+= 0.001) {
            if (*end) {
                break;
            }
            thread_lock.lock();
            *imaginary = i;
            thread_lock.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
        for (long double i = 0.5; i >= 0.28; i -= 0.001) {
            if (*end) {
                break;
            }
            thread_lock.lock();
            *imaginary = i;
            thread_lock.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
    }
}