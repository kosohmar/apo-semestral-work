#include <thread>
#include <iostream>
#include <mutex>

#include "display_painter.h"
#include "led_handler.h"
#include "presenter.h"
#include "menu_writer.h"
#include "knobs.h"

int main(int argc, char *argv[]) {
    // redraws display to the black
    paint_display_black();
    // LED strip blinking - indicates beginning of the program
    blink();
    // draw menu on the display
    draw_menu();

    bool presentation_mode;
    // wait for push knob choice input (red knob push = normal mode, green knob push = presentation mode)
    process_push_knobs_values(presentation_mode);

    // itit values for the julia set
    double real = -0.678;
    double imaginary = 0.3125;
    bool end = false;

    std::mutex mutex;
    std::thread painting_thread;
    std::thread presentation_thread;
    std::thread knobs_thread;
    // thread for paint julia set on the display
    painting_thread = std::thread(paint_julia, &real, &imaginary, &mutex, &end);
    if (presentation_mode) {
        // thread for changing (real and imaginary) values when presentation mode
        presentation_thread = std::thread(present_julia, &imaginary, &mutex, &end);
    } else {
        // thread for detecting changes in knobs and record them
        knobs_thread = std::thread(process_rotational_knobs_values, &real, &imaginary, &mutex, &end);
    }

    getchar();
    end = true;
    painting_thread.join();
    knobs_thread.join();
    if (presentation_mode) {
        presentation_thread.join();
    }
    // redraws display to the black
    paint_display_black();
    // LED strip blinking - indicates end of the program
    blink();
    return 0;
}