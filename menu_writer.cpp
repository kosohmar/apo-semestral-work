#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <iostream>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#define LCD_WIDTH 480
#define LCD_HEIGHT 320

unsigned short lcd_array[LCD_WIDTH * LCD_HEIGHT];
font_descriptor_t *fdes;

void draw_pixel(int x, int y, unsigned short color) {
  if (x >= 0 && x < 480 && y >= 0 && y < 320) {
    lcd_array[x + 480 * y] = color;
  }
}

void draw_pixel_big(int x, int y, unsigned short color, int scale) {
  int i, j;
  for (i = 0; i < scale; i++) {
    for (j = 0; j < scale; j++) {
      draw_pixel(x + i, y + j, color);
    }
  }
}

int char_width(int ch) {
  int width;
  if (!fdes->width) {
    width = fdes->maxwidth;
  } else {
    width = fdes->width[ch-fdes->firstchar];
  }
  return width;
}

void draw_char(int x, int y, char ch, unsigned short color, int scale) {
  int w = char_width(ch);
  const font_bits_t *ptr;
  if ((ch >= fdes -> firstchar) && (ch - fdes -> firstchar < fdes -> size)) {
    if (fdes -> offset) {
      ptr = &fdes -> bits[fdes -> offset[ch - fdes -> firstchar]];
    } else {
      int bw = (fdes -> maxwidth + 15)/16;
      ptr = &fdes -> bits[(ch - fdes -> firstchar) * bw * fdes -> height];
    }
    int i, j;
    for (i = 0; i < (int) fdes -> height; i++) {
      font_bits_t val = *ptr;
      for (j = 0; j < w; j++) {
        if ((val&0x8000) != 0) {
          draw_pixel_big(x + scale * j, y + scale * i, color, scale);
        }
        val <<= 1;
      }
      ptr++;
    }
  }
}

void draw_string(int x, int y, std::string str, unsigned short color, int scale) {
  int str_len = str.length();
  for(int i = 0; i < str_len; i++) {
    draw_char(x, y, str[i], color, scale);
    x += scale * char_width(str[i]);
  }
}

void draw_menu() {
  fdes = &font_winFreeSystem14x16;
  unsigned char *parlcd_mem_base;
  parlcd_mem_base = (unsigned char*) map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (parlcd_mem_base == NULL) {
    exit(EXIT_FAILURE);
  }
  parlcd_hx8357_init(parlcd_mem_base);

  draw_string(30, 30, "Welcome to Julia set", 0xffff, 3);
  draw_string(30, 100, "Press:", 0xffff, 2);
  draw_string(30, 150, "red knob to normal mode", 0xffff, 2);
  draw_string(30, 200, "green knob to presentation mode", 0xffff, 2);

  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  for (int i = 0; i < 480 * 320 ; i++) {
    parlcd_write_data(parlcd_mem_base, lcd_array[i]);
  }
}
