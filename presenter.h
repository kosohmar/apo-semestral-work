#ifndef PRESENTER_H
#define PRESENTER_H

#include <mutex>

void present_julia(double *imaginary, std::mutex *mutex, bool *end);

#endif /*PRESENTER_H*/