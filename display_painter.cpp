#include <iostream>
#include <chrono>
#include <thread>

#include "mzapo_parlcd.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "display_painter.h"

#define LCD_WIDTH 480
#define LCD_HEIGHT 320

double last_knob_real = 0;
double last_knob_imaginary = 0;
unsigned short lcd_display[LCD_WIDTH * LCD_HEIGHT];

void calculate_julia(double *real, double *imaginary) {
    if (last_knob_real == *real && last_knob_imaginary == *imaginary) {
        return;
    }

    last_knob_real = *real;
    last_knob_imaginary = *imaginary;

    const double small_factor = 4;
    const double iterations_limit = 300;
    const double imaginary_start = (small_factor * LCD_HEIGHT) / (LCD_WIDTH * -2);
    const double real_step = small_factor / LCD_WIDTH;
    const double imaginary_step = (-2 * imaginary_start) / LCD_HEIGHT;

    double last_imaginary, last_real, new_imaginary, new_real, completed_iterations, ratio;
    unsigned char r, g, b;

    for(int i = 0; i < LCD_HEIGHT; i++) {
        last_imaginary = i * imaginary_step + imaginary_start;
        for(int j = 0; j < LCD_WIDTH; j++) {
            last_real = j * real_step -2;
            new_real = last_real;
            new_imaginary = last_imaginary;
            for(completed_iterations = 0; completed_iterations < iterations_limit; completed_iterations++) {
                double real_pow = new_real * new_real;
                double imaginary_pow = new_imaginary * new_imaginary;
                if (real_pow + imaginary_pow > small_factor) {
                    break;
                }
                new_imaginary = 2 * new_real * new_imaginary + *imaginary;
                new_real = real_pow - imaginary_pow + *real;
            }
            ratio = completed_iterations/ iterations_limit;
            r = (unsigned char) 9 * (1 - ratio) * ratio * ratio * ratio * 255;
            g = (unsigned char) 15 * (1 - ratio) * (1 - ratio) * ratio * ratio * 255;
            b = (unsigned char) 8.5 * (1 - ratio) * (1 - ratio) * (1 - ratio) * ratio * 255;
            lcd_display[i * LCD_WIDTH + j] = ((r & 0xf8) << 8) | ((g & 0xfc) << 3) | ((b & 0xf8) >> 3);
        }
    }
}

void paint_display(unsigned char *parlcd_mem_base) {
  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  for (int i = 0; i < LCD_WIDTH * LCD_HEIGHT; i++) {
    parlcd_write_data(parlcd_mem_base, lcd_display[i]);
  }
}

void paint_julia(double *real, double *imaginary, std::mutex *mutex, bool *end) {
    unsigned char *parlcd_mem_base;
    parlcd_mem_base = (unsigned char*) map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL) {
        exit(EXIT_FAILURE);
    }
    parlcd_hx8357_init(parlcd_mem_base);
    std::unique_lock <std::mutex> thread_lock(*mutex);
    thread_lock.unlock();

    while(!*end) {
        thread_lock.lock();
        calculate_julia(real, imaginary);
        paint_display(parlcd_mem_base);
        thread_lock.unlock();
    }
}

void paint_display_black() {
    unsigned char *parlcd_mem_base;
    parlcd_mem_base = (unsigned char*) map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL) {
        exit(EXIT_FAILURE);
    }
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int i = 0; i < LCD_WIDTH * LCD_HEIGHT; i++) {
    parlcd_write_data(parlcd_mem_base, 0);
  }       
}