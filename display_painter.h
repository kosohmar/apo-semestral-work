#ifndef DISPLAY_PAINTER_H
#define DISPLAY_PAINTER_H

#include <mutex>

void paint_julia(double *constant_real, double *constant_imaginary, std::mutex *mutex, bool *end);

void paint_display_black();

#endif /*DISPLAY_PAINTER_H*/
