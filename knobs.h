#ifndef KNOBS_H
#define KNOBS_H

#include <mutex>

void process_rotational_knobs_values(double *real, double *imaginary, std::mutex *mutex, bool *end);

void process_push_knobs_values(bool& presentation_mode);

#endif /*KNOBS_H*/
