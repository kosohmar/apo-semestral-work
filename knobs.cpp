#include <iostream>
#include <chrono>
#include <thread>

#include "mzapo_parlcd.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "knobs.h"

uint32_t get_rotational_knobs_value(unsigned char *mem_base) {
  if (mem_base == NULL) {
    exit(1);
  }
  uint32_t rgb_knobs_value = *(volatile uint32_t *) (mem_base + SPILED_REG_KNOBS_8BIT_o);
  return rgb_knobs_value;
}

uint32_t get_push_knobs_value(unsigned char *mem_base) {
  if (mem_base == NULL) {
    exit(1);
  }
  uint32_t rgb_knobs_value = *(volatile uint32_t *) (mem_base + SPILED_REG_KBDRD_KNOBS_DIRECT_o);
  return rgb_knobs_value;
}

long double divide_value(long double knob_value) {
  return (knob_value - 128.0) / 64.0;
}

void process_push_knobs_values(bool& presentation_mode) {
    unsigned char *mem_base;
    mem_base = (unsigned char*) map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    uint32_t green_knob_value = 0;
    uint32_t red_knob_value = 0;
    while(!green_knob_value && !red_knob_value) {
      uint32_t rgb_knobs_value = get_push_knobs_value(mem_base);
      green_knob_value = rgb_knobs_value & (1 << 21);
      red_knob_value = rgb_knobs_value & (1 << 24);
      if (red_knob_value > 0) {
          presentation_mode = false;
      } else if (green_knob_value > 0) {
          presentation_mode = true;
      }
    }
}

void process_rotational_knobs_values(double *real, double *imaginary, std::mutex *mutex, bool *end) {
    unsigned char *mem_base;
    mem_base = (unsigned char*) map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    std::unique_lock <std::mutex> thread_lock(*mutex);
    thread_lock.unlock();
    while(!*end) {
        thread_lock.lock();
        uint32_t rgb_knobs_value = get_rotational_knobs_value(mem_base);
        *real = divide_value((uint8_t)(rgb_knobs_value >> 16)); // red knob
        *imaginary = divide_value((uint8_t)(rgb_knobs_value >> 8)); // green knob
        thread_lock.unlock();
    }
}